
/**
 * ImageCrop.js
 * 2022-09-05
 * https://gitee.com/Yun08/image-crop
 */


(function (global, factory) {
    if (typeof module !== 'undefined' && typeof exports === 'object') {
        module.exports = factory
    } else if (typeof define === 'function' && (define.cmd || define.amd)) {
        define(factory)
    } else {
        global.ImageCrop = factory
    }
})(this, (function () {

    let _imageElement_ = null,//图片对象
        _canvasElement_ = null,//画布对象
        _cropPositY_ = 0,//裁剪位置计算,
        _cropPositX_ = 0,
        _clientY_ = 0,//图片移动坐标
        _clientX_ = 0,
        _zoom_ = 1;//缩放等级

    class ImageCrop {
        constructor(pops) {
            this.el;//inputElement对象
            this.canvasWidth = 900;//画布大小
            this.canvasHeight = 450;
            this.cropWidth = 600;//裁剪框大小
            this.cropHeight = 300;
            this.cropBtn = "#crop";//裁剪按钮
            this.cropResetBtn = "#crop-reset";//裁剪关闭按钮
            this.onCropReset = null;//点击关闭
            this.onCrop = null;//点击裁剪
            this.onChange = null;//监听数据

            Object.assign(this, pops);
            if (typeof this.canvasWidth !== "number") return console.error("invalid 'canvasWidth' not number type")
            if (typeof this.canvasHeight !== "number") return console.error("invalid 'canvasHeight' not number type")
            if (typeof this.cropWidth !== "number") return console.error("invalid 'cropWidth' not number type")
            if (typeof this.cropHeight !== "number") return console.error("invalid 'cropHeight' not number type")
            // 初始化界面
            this.__createCanvas__();
            // 监听input
            this.el.addEventListener('change', async () => {
                if (!this.el.files.length) return;
                let file = this.el.files[0];
                this.__createCanvas__(this.__createObjectURL__(file));
            })
        }
        // 生成图片数据
        __imageCrop__(crop) {
            let ctx = document.createElement('canvas');
            ctx.width = this.cropWidth;
            ctx.height = this.cropHeight;
            ctx.innerText = '您的浏览器不支持 HTML5 canvas 标签。';
            // 使用 _clientX_ - _cropPositX_;_clientY_ - _cropPositY_ 获取正确裁剪的开始位置;
            let image = _imageElement_;
            let cropPositX = _clientX_ - _cropPositX_;
            let cropPositY = _clientY_ - _cropPositY_;
            this.__clearRect__(ctx).drawImage(image, cropPositX, cropPositY, image.width, image.height);
            // 点击裁剪回调方法
            if (crop) return this.onCrop(ctx.toDataURL(), this.__base64ToBlob__(ctx.toDataURL(), `${Date.now()}.png`));
            // 实时监听回调
            if (typeof this.onChange === "function") this.onChange(ctx.toDataURL())
        }

        // 创建裁剪视图
        __createCropView__() {
            let canvasHtml = `<div id="canvas">
                <div id="canvas-view" style="width:${this.cropWidth}px;height:${this.cropHeight}px">
                    <div id="bg-t"></div>
                    <div id="bg-b"></div>
                    <div id="bg-l"></div>
                    <div id="bg-r"></div>
                </div>
            </div>
            <div id="crop-btn-item">
                ${this.cropBtn == '#crop' ? `<div id="crop">裁剪</div>` : ''}
                ${this.cropResetBtn == '#crop-reset' ? `<div id="crop-reset">重置</div>` : ''}
            </div>
            `

            document.querySelector("#imageCrop").innerHTML = canvasHtml;

            //画布的大小-裁剪框的大小=剩余的大小/2 (居中) 
            _cropPositY_ = (this.canvasHeight - this.cropHeight) / 2;
            _cropPositX_ = (this.canvasWidth - this.cropWidth) / 2;

            // 裁剪
            this.cropBtn = this.cropBtn == "#crop" ? document.querySelector("#imageCrop #crop") : this.cropBtn;
            this.cropBtn.addEventListener("click", ev => {
                if (typeof this.onCrop === "function") this.__imageCrop__(true);
            }, false);

            // 关闭
            this.cropResetBtn = this.cropResetBtn == '#crop-reset' ? document.querySelector("#imageCrop #crop-reset") : this.cropResetBtn;
            this.cropResetBtn.addEventListener("click", ev => {
                this.el.value = '';
                this.__clearRect__(_canvasElement_);
                _imageElement_ = null;
                if (typeof this.onCropReset === "function") this.onCropReset();
            }, false);
        }
        // 创建canvas背景图
        async __createCanvas__(imgUrl) {
            if (!_canvasElement_) {
                this.__createCropView__();
                let canvasFrame = document.querySelector("#canvas");
                canvasFrame.style.width = this.canvasWidth + "px";
                canvasFrame.style.height = this.canvasHeight + "px";
                let imageCanvas = document.createElement('canvas');
                imageCanvas.width = this.canvasWidth;
                imageCanvas.height = this.canvasHeight;
                _canvasElement_ = imageCanvas;
                canvasFrame.appendChild(imageCanvas);
                this.__dragEvent__();
                this.__zoomEvent__();
            }
            if (!imgUrl) return;
            _clientY_ = 0;//初始化图片移动坐标
            _clientX_ = 0;
            _zoom_ = 1;
            _imageElement_ = await new Promise((resolve, reject) => {
                let imageObj = new Image();
                imageObj.setAttribute("crossOrigin", 'Anonymous')
                imageObj.src = imgUrl;
                imageObj.onload = () => {
                    resolve(imageObj)
                }
            })
            this.__drawImage__(_canvasElement_, _imageElement_, 0, 0)
            // 回调加载完成方法
            if (typeof this.onComplete === "function") this.onComplete();
            return _imageElement_;
        }
        //清除画布
        __clearRect__(canvas) {
            let ctx = canvas.getContext('2d')
            ctx.clearRect(0, 0, canvas.width, canvas.height)
            return ctx
        }
        // 图片渲染
        __drawImage__(canvas, image, clientX, clientY) {
            this.__clearRect__(canvas).drawImage(image, clientX, clientY, image.width, image.height);
            if (typeof this.onChange === "function") this.__imageCrop__();
        }
        __scaleImage__(canvas, image, zoom) {
            image.width = image.naturalWidth * zoom;
            image.height = image.width / (image.naturalWidth / image.naturalHeight);//等比缩放
            this.__drawImage__(canvas, image, _clientX_, _clientY_);
        }
        __createObjectURL__(file) {
            let url = null;
            if (window.createObjectURL !== undefined) { // basic
                url = window.createObjectURL(file);
            } else if (window.webkitURL !== undefined) { // webkit or chrome
                url = window.webkitURL.createObjectURL(file);
            } else if (window.URL !== undefined) { // mozilla(firefox)
                url = window.URL.createObjectURL(file);
            }
            return url;
        }
        __base64ToBlob__(base64, fileName) {
            var arr = base64.split(","),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = window.atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);
            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }
            return new File([u8arr], fileName, {
                type: mime,
            });
        }
        __zoomEvent__() {
            let wheel = (ev) => {
                if (!_imageElement_) return;
                wheel = ev.wheelDelta ? ev.wheelDelta : (ev.detail > 0 ? -1 : 1);
                if (wheel < 0) {
                    let zoom = _zoom_ * 0.9;
                    _zoom_ = zoom > 0.1 ? zoom : 0.1;
                }

                if (wheel > 0) {
                    let zoom = _zoom_ / 0.9;
                    _zoom_ = zoom > 5 ? 5 : zoom;
                }
                this.__scaleImage__(_canvasElement_, _imageElement_, _zoom_);
            }
            let canvasBox = document.querySelector("#canvas");
            canvasBox.addEventListener("mousewheel", wheel, false);
            canvasBox.addEventListener("DOMMouseScroll", wheel, false);
        }

        __dragEvent__() {
            let canvasBox = document.querySelector("#canvas");
            let clientY = _clientY_;
            let clientX = _clientX_;
            let downY = 0;
            let downX = 0;
            let move = (ev) => {
                _clientY_ = ev.clientY - downY + clientY;
                _clientX_ = ev.clientX - downX + clientX;
                this.__drawImage__(_canvasElement_, _imageElement_, _clientX_, _clientY_);
            }
            canvasBox.addEventListener("mousedown", ev => {
                clientY = _clientY_;
                clientX = _clientX_;
                downY = ev.clientY;
                downX = ev.clientX;
                if (!_imageElement_) return;
                document.addEventListener("mousemove", move, false);
                // 移动
            }, false);
            document.addEventListener("mouseup", e => {
                document.removeEventListener("mousemove", move, false)
            }, false);
        }

    }
    return ImageCrop;
})());

