### 介绍
JavaScript原生裁剪工具，支持滚轮缩放；

![输入图片说明](static/image.png)

### 软件架构

JavaScript

### 使用说明


>    vue 需引入src目录的 image-crop文件
```
import ImageCrop 'js/image-crop.js';
import 'css/image-crop.css';
```

> 普通只需引入 dist 里的文件即可使用
```

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/image-crop.css" rel="stylesheet">
    <title>ImageCrop v2.2</title>
    <style>
        html,
        body {
            background: #dbdbdb;
        }

        img {
            background: #FFF;
        }
    </style>
</head>

<body>
    <input id="file" type="file">
    <img id="show">
    <div id="imageCrop"></div>

</body>
<script src="./js/image-crop.js"></script>
<script>
    let imageCrop = new ImageCrop({
        el: document.querySelector("#file"),
        canvasWidth: 900,
        canvasHeight: 500,
        cropWidth: 600,
        cropHeight: 300,
        // cropBtn: document.querySelector(".crop"),
        // cropResetBtn: document.querySelector(".reset")
    });

    imageCrop.onComplete = () => { }

    imageCrop.onChange = (base64) => {
        document.querySelector("#show").src = base64;
    }

    imageCrop.onCrop = (base64, file) => {
        document.querySelector("#show").src = base64;
    }
    imageCrop.onCropReset = () => {
        console.log("reset")
    }
</script>

</html>

```


| 属性         | 类型    | 默认值      | 返回值       | 说明                            |
| ------------ | ------- | ----------- | ------------ | ------------------------------- |
| el           | element |             |              | element                         |
| cropWidth    | number  | 600         |              | 裁剪框 宽度                     |
| cropHeight   | number  | 300         |              | 裁剪框 高度                     |
| canvasWidth  | number  | 900         |              | 画布 宽度                       |
| canvasHeight | number  | 450         |              | 画布 高度                       |
| cropBtn      | element | #crop       |              | element，自定义后会隐藏默认按钮 |
| cropCloseBtn | element | #crop-close |              | element，自定义后会隐藏默认按钮 |
| onComplete   | fn      |             |              | 图片加载完成回调                |
| onChange     | fn      |             | base64       | 实时数据回调                    |
| onCrop       | fn      |             | base64, blob | 裁剪回调                        |
| onCropReset  | fn      |             |              | 重置回调                        |
