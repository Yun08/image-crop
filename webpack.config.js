let CopyPlugin = require('copy-webpack-plugin');
module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "./js/image-crop.js"
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                "@babel/preset-env",
                                // [
                                //     "@babel/preset-env"
                                // ]
                            ],
                            // assumptions: {
                            //     "setPublicClassFields": true
                            // },
                            plugins: [
                                // [require("@babel/plugin-transform-runtime"), { "legacy": true }],
                                // [require("@babel/plugin-proposal-class-properties")]
                            ]
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: "./src", to: "",
                    globOptions: {
                        ignore: [ // 忽略的文件
                            '**/index.js',
                            '**/image-crop.js'
                        ]
                    }
                },
            ],
        }),
    ]
}